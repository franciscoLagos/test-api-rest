**Proyecto para test tecnico en falabella**

Este es un Proyecto de prueba que crea una api rest en nodejs con un servicio post que almacena en una archivo
los datos ingresados por el cloente.

---

## La forma de inicializar el proyecto es 


1. Clonar el proyecto en tu directorio de preferencia
2. Dirigirse donde se clono el proyecto, entrar a la carpeta y dar npm install
3. colocar en la consola node server/server
3. Luego de eso la Api Rest de desplegara en el puerto **3000**.
---

## Creacion del archivo

Cada vez que se da continuar en la APP del frontend se almacena temporalmente en un archivo la informacion enviada.
Dicho archivo queda en el directorio principal con nombre infoCustomer.txt

---
