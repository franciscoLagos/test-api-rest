require('./config/config');

const express = require('express')
const app = express();
const bodyParser = require('body-parser');


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

//referencia al index -> config a las rutas
app.use(require('./routes/index'));

var cors = require('cors');

// use it before all route definitions
app.use(cors({ origin: 'localhost' }));

app.listen(process.env.PORT, () => {

})