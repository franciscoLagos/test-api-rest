//verifica datos customer
const express = require('express')
const app = express();

const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

let verificaData = (req, res, next) => {
    let body = req.body;
    const client = {
        rut: body.rut,
        celular: body.celular,
        correo: body.correo,
        renta: body.renta
    }


    if ((client.rut === null || client.rut === undefined) || (client.celular === null || client.celular === undefined) || (client.correo === null || client.correo === undefined) || (client.renta === null || client.renta === undefined)) {
        return res.status(500).json({
            ok: false,
            error: 'Falta un parametro'
        })
    }
    next();

    /*
    res.json({
        client
    });*/



};


module.exports = {
    verificaData
}