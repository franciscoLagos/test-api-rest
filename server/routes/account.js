const express = require('express')
const app = express();
const fs = require('fs');

const { verificaData } = require('../middlewares/verification')
var cors = require('cors');

// use it before all route definitions
app.use(cors({ origin: '*' }));
app.post('/account', verificaData, (req, res) => {

    let body = req.body;

    if (body !== undefined) {
        createFile(body);
        res.json({
            response: 'Archivo Creado correctamente con los datos del cliente en el directorio principal'
        });
    }


})


let createFile = (body) => {
    var stream = fs.createWriteStream("infoCustomer.txt");
    stream.once('open', function(fd) {
        stream.write("Rut:");
        stream.write(body.rut + '\n');
        stream.write("Celular:");
        stream.write(body.celular + '\n');
        stream.write("Correo:");
        stream.write(body.correo + '\n');
        stream.write("Renta");
        stream.write(body.renta + '\n');
        stream.end();
    });

}


module.exports = app;